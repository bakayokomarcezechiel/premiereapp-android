package com.example.premiereapp

import android.app.Activity
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.Image
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.material3.windowsizeclass.ExperimentalMaterial3WindowSizeClassApi
import androidx.compose.material3.windowsizeclass.WindowSizeClass
import androidx.compose.material3.windowsizeclass.WindowWidthSizeClass
import androidx.compose.material3.windowsizeclass.calculateWindowSizeClass
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import androidx.navigation.compose.rememberNavController

@Composable
fun Profil(windowSizeClass: WindowSizeClass,
           navController : NavController
) {
    Screen(windowClass = windowSizeClass, navController)
}

@Composable
fun Screen(windowClass: WindowSizeClass, navController : NavController
) {
    when (windowClass.widthSizeClass) {
        WindowWidthSizeClass.Compact -> {
            Column(Modifier.fillMaxSize(),
                verticalArrangement = Arrangement.SpaceEvenly,
                horizontalAlignment = Alignment.CenterHorizontally) {
                col1(navController)
            }
        }
        else -> {
            Row(Modifier.fillMaxSize(),
                verticalAlignment = Alignment.CenterVertically) {
                col1(navController)
            }
        }
    }
}


val ArabotoFamilly = FontFamily(
    Font(R.font.araboto_normal, FontWeight.Normal),
    Font(R.font.araboto_light, FontWeight.Light),
    Font(R.font.araboto_bold, FontWeight.Bold),
    Font(R.font.araboto_black, FontWeight.Black, FontStyle.Italic),
    Font(R.font.araboto_medium, FontWeight.Medium),
)



val borderWidth = 3.dp
@Composable
fun col1(navController: NavController) {
    Column(
        horizontalAlignment = Alignment. CenterHorizontally
    ) {
        Image(
            painterResource(id = R.drawable.prolile),
            contentDescription = "Marc",
            modifier = Modifier
                .size(160.dp)
                .border(
                    BorderStroke(borderWidth, Color.Gray),
                    CircleShape
                )
                .clip(CircleShape)

        )
        Text(
            text = " Bakayoko Marc E. ",
            fontFamily = ArabotoFamilly,
            fontWeight = FontWeight.Medium,
            fontSize = 35.sp,
            color = Color.DarkGray,

            )

        Text(
            text = " Etudiant à l'ecole d'ingénieur ISIS",
            fontFamily = FontFamily.Monospace,
            fontStyle = FontStyle.Italic,
            color = Color.DarkGray,
        )
    }
    col2(navController)
}


operator fun Unit.invoke(modifier: Modifier, verticalArrangement: Arrangement.Vertical, horizontalAlignment: Alignment.Horizontal, value: () -> Unit) {

}


@Composable
fun col2(navController: NavController) {
    Column(
        horizontalAlignment = Alignment. CenterHorizontally
    ) {
        Row(
            verticalAlignment = Alignment.CenterVertically
        )
        {
            Image(
                painterResource(id = R.drawable.mail),
                contentDescription = "Marc",
                modifier = Modifier
                    .size(40.dp)
                    .padding(2.dp)
            )
            Text(
                text = "bakezechiel3231@gmail.com",
                fontStyle = FontStyle.Italic,
                fontSize = 20.sp,
            )
        }
        Row(
            verticalAlignment = Alignment.CenterVertically
        ) {
            Image(
                painterResource(id = R.drawable.linkedinlogo),
                contentDescription = "Marc",
                modifier = Modifier
                    .size(40.dp)
                    .padding(2.dp)
            )
            Text(
                text = "www.linkedin/in/bakezechiel",
                fontStyle = FontStyle.Italic,
                fontSize = 20.sp,
            )
        }
        Spacer(modifier = Modifier.height(40.dp))
        Row() {
            Button(onClick = {
                navController.navigate(route = Ecran.EcranPrincipal.itineraire)
            }) {
                Text(
                    text = "Démarrer"
                )
            }
        }
    }

}

@OptIn(ExperimentalMaterial3WindowSizeClassApi::class)
@Preview(showBackground = true)
@Composable
fun ProfilPreview() {
        Profil(calculateWindowSizeClass(activity = Activity()),
            navController = rememberNavController()
        )
}