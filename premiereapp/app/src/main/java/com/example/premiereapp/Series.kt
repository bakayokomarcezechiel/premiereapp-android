package com.example.premiereapp

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.lazy.grid.items
import androidx.compose.material3.Card
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.compose.AsyncImage

@Composable
fun Serie(viewModel: MainViewModel){
    SerieSemaine(viewModel)
}


@Composable
fun SerieSemaine(viewModel: MainViewModel){

    var detail by remember {
        mutableStateOf(0)
    }
    var idserie by remember {
        mutableStateOf("")
    }
    if (detail == 0){
        val series by viewModel.serie.collectAsState()

        if (series.isEmpty()) viewModel.getWeekSerie()
        LazyVerticalGrid(
            columns = GridCells.Fixed(2),
            horizontalArrangement = Arrangement.spacedBy(16.dp),
            verticalArrangement = Arrangement.spacedBy(12.dp)
        ) {
            items(series){ serie -> SerieCard(serie = serie, {detail = it}) {
                idserie = it
            }
            }

        }
    }else{
        SerieDetails(viewModel = MainViewModel(), id = idserie)
    }

}

@Composable
fun RechercheSerie(name: String, viewModel: MainViewModel){
    val series by viewModel.serie.collectAsState()
    var detail by remember {
        mutableStateOf(0)
    }
    var idacteurs by remember {
        mutableStateOf("")
    }
    if (series.isEmpty()) viewModel.searchSerie(name)
    LazyVerticalGrid(
        columns = GridCells.Fixed(2),
        horizontalArrangement = Arrangement.spacedBy(16.dp),
        verticalArrangement = Arrangement.spacedBy(12.dp)
    ) {
        items(series){ serie -> SerieCard(serie = serie, {detail = it}) { idacteurs = it }
        }

    }
}

@Composable
fun SerieCard(serie: Serie, detailUpdate:(detail: Int) -> Unit, idserieUpdate: (idserie: String) -> Unit) {
    var detail by remember {
        mutableStateOf(0)
    }

    var idserie by remember {
        mutableStateOf("")
    }

    Card(
        modifier = Modifier
            .fillMaxWidth()
            .fillMaxSize()
            .wrapContentHeight(),
        //elevation = 8.dp,
    ) {
        Column(
            modifier = Modifier
                .padding(16.dp)
                .fillMaxSize()
                .clickable {
                    detail = 1
                    detailUpdate(detail)
                    idserie = serie.id.toString()
                    idserieUpdate(idserie)
                }
            ,
            verticalArrangement = Arrangement.spacedBy(8.dp)

        ) {
            // Image de couverture
            if (serie.backdrop_path == null){
                CoverImageSerie(imageUrl = "https://media.licdn.com/dms/image/C4D03AQHosBjWh4JapA/profile-displayphoto-shrink_800_800/0/1638740735138?e=1703116800&v=beta&t=AbBUBYo2U1Uhm3xs4dhWKV8ypsG_7jboIb-q5JuvgIM")
            } else {
                CoverImageSerie(imageUrl = serie.backdrop_path)
            }

            // Titre du film
            Text(
                text = serie.name,
                style = TextStyle(
                    fontSize = 18.sp,
                    fontWeight = FontWeight.Bold
                )
            )
        }
    }
}

@Composable
fun CoverImageSerie(imageUrl: String) {
    AsyncImage(
        model = "https://image.tmdb.org/t/p/w342/$imageUrl",
        contentDescription = null,
        contentScale = ContentScale.FillHeight,
        modifier = Modifier
            .fillMaxWidth()
            .height(200.dp) // Ajustez la hauteur de l'image en mode portrait
    )
}
