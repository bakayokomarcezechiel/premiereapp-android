package com.example.premiereapp

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.lazy.grid.items
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.material3.Card
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.compose.AsyncImage

@Composable
fun Acteurs(viewModel: MainViewModel){
    ActeursSemaine(viewModel)
}


@Composable
fun ActeursSemaine(viewModel: MainViewModel){
    var detail by remember {
        mutableStateOf(0)
    }

    var idacteurs by remember {
        mutableStateOf("")
    }

    if (detail == 0){
        val acteurs by viewModel.person.collectAsState()

        if (acteurs.isEmpty()) viewModel.getWeekPerson()
        LazyVerticalGrid(
            columns = GridCells.Fixed(2),
            horizontalArrangement = Arrangement.spacedBy(16.dp),
            verticalArrangement = Arrangement.spacedBy(12.dp)
        ) {
            items(acteurs){ acteur -> ActeursCard(acteur = acteur, {detail = it}) {
                idacteurs = it
            }
            }

        }
    }else{
        //ActeursDetails(viewModel = MainViewModel(), id = idacteurs)
    }

}

@Composable
fun RechercheActeurs(name: String, viewModel: MainViewModel){
    val acteurs by viewModel.person.collectAsState()
    var detail by remember {
        mutableStateOf(0)
    }
    var idacteurs by remember {
        mutableStateOf("")
    }
    if (acteurs.isEmpty()) viewModel.searchPerson(name)
    LazyVerticalGrid(
        columns = GridCells.Fixed(2),
        horizontalArrangement = Arrangement.spacedBy(16.dp),
        verticalArrangement = Arrangement.spacedBy(12.dp)
    ) {
        items(acteurs){ acteur -> ActeursCard(acteur = acteur, {detail = it}) { idacteurs = it }
        }

    }
}

@Composable
fun ActeursCard(acteur: Person, detailUpdate:(detail: Int) -> Unit, idacteurUpdate: (idacteur: String) -> Unit) {
    var detail by remember {
        mutableStateOf(0)
    }

    var idacteur by remember {
        mutableStateOf("")
    }

    Card(
        modifier = Modifier
            .fillMaxWidth()
            .fillMaxSize()
            .wrapContentHeight(),
        //elevation = 8.dp,
    ) {
        Column(
            modifier = Modifier
                .padding(16.dp)
                .fillMaxSize()
                .clickable {
                    detail = 1
                    detailUpdate(detail)
                    idacteur = acteur.id.toString()
                    idacteurUpdate(idacteur)
                }
            ,
            verticalArrangement = Arrangement.spacedBy(8.dp)

        ) {
            // Image de couverture
            if (acteur.profile_path == null){
                CoverImageActeur(imageUrl = "https://media.licdn.com/dms/image/C4D03AQHosBjWh4JapA/profile-displayphoto-shrink_800_800/0/1638740735138?e=1703116800&v=beta&t=AbBUBYo2U1Uhm3xs4dhWKV8ypsG_7jboIb-q5JuvgIM")
            } else {
                CoverImageActeur(imageUrl = acteur.profile_path)
            }

            // Titre du film
            Text(
                text = acteur.name,
                style = TextStyle(
                    fontSize = 18.sp,
                    fontWeight = FontWeight.Bold
                )
            )
        }
    }
}

@Composable
fun CoverImageActeur(imageUrl: String) {
    AsyncImage(
        model = "https://image.tmdb.org/t/p/w342/$imageUrl",
        contentDescription = null,
        contentScale = ContentScale.FillHeight,
        modifier = Modifier
            .fillMaxWidth()
            .height(200.dp) // Ajustez la hauteur de l'image en mode portrait
    )
}

