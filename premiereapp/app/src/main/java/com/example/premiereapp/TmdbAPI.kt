package com.example.premiereapp

import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface TmdbAPI {
    @GET("trending/movie/week")
    suspend fun getFilmsSemaine(@Query("api_key")  apikey: String, @Query("language") language: String) :TmdbResult

    @GET("trending/person/week")
    suspend fun getActeursSemaine(@Query("api_key")  apikey: String, @Query("language") language: String) :PersonResult

    @GET("trending/tv/week")
    suspend fun getSerieSemaine(@Query("api_key")  apikey: String, @Query("language") language: String) :SerieResult

    @GET("search/movie")
    suspend fun getFilmsParMotCle(@Query("api_key")  apikey: String, @Query("query") motcle: String) : TmdbResult

    @GET("search/person")
    suspend fun getActeursParMotCle(@Query("api_key")  apikey: String, @Query("query") motcle: String) : PersonResult


    @GET("search/tv")
    suspend fun getSerieParMotCle(@Query("api_key")  apikey: String, @Query("query") motcle: String) : SerieResult


    @GET("movie/{id}?append_to_response=credits")
    suspend fun getFilmsDetails(@Path("id") id: String, @Query("api_key") apikey: String , @Query("language") language: String):MoviesDetail


    @GET("tv/{id}?append_to_response=credits")
    suspend fun getSerieDetails(@Path("id") id: String, @Query("api_key") apikey: String , @Query("language") language: String):SerieDetailResult



}