package com.example.premiereapp

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.lazy.grid.items
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.lifecycle.ViewModel
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.Card
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.compose.AsyncImage

@Composable
fun Film(viewModel: MainViewModel){
    FilmSemaine(viewModel)
}


@Composable
fun FilmSemaine(viewModel: MainViewModel){
    var detail by remember {
        mutableStateOf(0)
    }

    var idfilms by remember {
        mutableStateOf("")
    }

    if (detail == 0){
        val movies by viewModel.movies.collectAsState()
        if (movies.isEmpty()) viewModel.getWeekMovies()
        LazyVerticalGrid(
            modifier = Modifier.fillMaxSize(),
            columns = GridCells.Fixed(2),
            horizontalArrangement = Arrangement.spacedBy(16.dp),
            verticalArrangement = Arrangement.spacedBy(12.dp)
        ) { items(movies){ movie -> MovieCard(movie = movie, {detail = it}, {idfilms = it}) }
        }
    }else{
            FilmsDetails(viewModel = MainViewModel(), id = idfilms)
        }

}

@Composable
fun RechercheFilms(name: String, viewModel: MainViewModel){
    val movies by viewModel.movies.collectAsState()
    var detail by remember {
        mutableStateOf(0)
    }
    var idfilms by remember {
        mutableStateOf("")
    }
    if (movies.isEmpty()) viewModel.searchMovies(name)
    LazyVerticalGrid(
        modifier = Modifier.fillMaxSize(),
        columns = GridCells.Fixed(2),
        horizontalArrangement = Arrangement.spacedBy(16.dp),
        verticalArrangement = Arrangement.spacedBy(12.dp)
    ) {
        items(movies){ movie -> MovieCard(movie = movie, {detail = it}, {idfilms = it} )
        }

    }
}

@Composable
fun MovieCard(movie : Movie, detailUpdate:(detail: Int) -> Unit, idfilmUpdate: (idfilm: String) -> Unit) {
    var detail by remember {
        mutableStateOf(0)
    }

    var idfilm by remember {
        mutableStateOf("")
    }

    Card(
        modifier = Modifier
            .fillMaxWidth()
            .fillMaxSize()
            .wrapContentHeight(),
        //elevation = 8.dp,
    ) {
        Column(
            modifier = Modifier
                .padding(16.dp)
                .fillMaxSize()
                .clickable {
                    detail = 1
                    detailUpdate(detail)
                    idfilm = movie.id.toString()
                    idfilmUpdate(idfilm)
                }
            ,
            verticalArrangement = Arrangement.spacedBy(8.dp)

        ) {
            // Image de couverture
            if (movie.backdrop_path == null){
                CoverImage(imageUrl = "https://media.licdn.com/dms/image/C4D03AQHosBjWh4JapA/profile-displayphoto-shrink_800_800/0/1638740735138?e=1703116800&v=beta&t=AbBUBYo2U1Uhm3xs4dhWKV8ypsG_7jboIb-q5JuvgIM")
            } else {
                CoverImage(imageUrl = movie.backdrop_path)
            }

            // Titre du film
            Text(
                text = movie.original_title,
                style = TextStyle(
                    fontSize = 18.sp,
                    fontWeight = FontWeight.Bold
                )
            )

            // Date de publication
            Text(
                text = " ${movie.release_date}",
                style = TextStyle(fontSize = 17.sp)
            )
        }
    }
}

@Composable
fun CoverImage(imageUrl: String) {
    AsyncImage(
        model = "https://image.tmdb.org/t/p/w342/$imageUrl",
        contentDescription = null,
        contentScale = ContentScale.FillHeight,
        modifier = Modifier
            .fillMaxWidth()
            .height(200.dp) // Ajustez la hauteur de l'image en mode portrait
    )

}

