package com.example.premiereapp

sealed class Ecran( val itineraire : String){

    object Profil : Ecran("profil_screen")
    object EcranPrincipal : Ecran("principal_screen")
    object Films : Ecran("films_ecran")
    object DetailFilms : Ecran("detailfilms_screen")
    object Serie : Ecran("serie_ecran")
    object DetailSerie : Ecran("detailserie_screen")
    object Acteurs : Ecran("acteurs_ecran")
    object DetailActeur : Ecran("detailacteurs_screen")


}
