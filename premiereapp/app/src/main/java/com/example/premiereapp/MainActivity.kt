package com.example.premiereapp

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.windowsizeclass.ExperimentalMaterial3WindowSizeClassApi
import androidx.compose.material3.windowsizeclass.calculateWindowSizeClass
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.navigation.compose.rememberNavController
import com.example.premiereapp.ui.theme.PremiereappTheme
import androidx.navigation.NavHostController


class MainActivity : ComponentActivity() {

    lateinit var navController: NavHostController

    @OptIn(ExperimentalMaterial3WindowSizeClassApi::class)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val viewmodel : MainViewModel by viewModels()

        setContent {
            val windowSizeClass = calculateWindowSizeClass(this)
            PremiereappTheme {
               /* Surface (
                    modifier= Modifier.fillMaxSize()
                ){
                    Greeting("Hobbit", viewmodel)
                }*/
                //Navigation()
                // A surface container using the 'background' color from the theme
                    navController =  rememberNavController()
                    Navigation(navController = navController,windowSizeClass,viewmodel)
            }
        }
    }
}
/*
@Composable
fun Greeting(name: String, viewModel: MainViewModel) {
    val movies by viewModel.movies.collectAsState()

    if(movies.isEmpty())   viewModel.searchMovies(name)

    LazyColumn{
        items(movies){
                movies -> Text(text = movies.original_title)
        }
    }
}

@Preview(showBackground = true)
@Composable
fun DefaultPreview(){
    PremiereappTheme{
        Greeting(name = "Android", viewModel = MainViewModel() )
    }
}

*/
// key Films : 3fa3f7d8ba0a4f2619cd704b65eb8795