package com.example.premiereapp
import androidx.compose.material3.windowsizeclass.WindowSizeClass
import androidx.compose.runtime.Composable
import androidx.navigation.NavHostController

import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable

@Composable
fun Navigation(
    navController: NavHostController,
    windowSizeClass: WindowSizeClass,
    viewmodel: MainViewModel
){
    NavHost(
        navController = navController,
        startDestination = Ecran.Profil.itineraire
    ){
        composable(
            route= Ecran.Profil.itineraire
        ){
            Profil(windowSizeClass,navController)
        }
        composable(route= Ecran.Films.itineraire){
            Film(viewmodel)
        }
        composable(route= Ecran.DetailFilms.itineraire){
            FilmsDetails(viewmodel,id= String())
        }

        composable(route= Ecran.Serie.itineraire){
            Serie(viewmodel)
        }

        composable(route= Ecran.DetailSerie.itineraire){
            SerieDetails(viewmodel,id= String())
        }

        composable(route= Ecran.Acteurs.itineraire){
            Acteurs(viewmodel)
        }
        composable(route= Ecran.DetailActeur.itineraire){
            ActeursDetails(viewmodel,id= String())
        }

        composable(route= Ecran.EcranPrincipal.itineraire){
            EcranPrincipal(windowSizeClass)
        }
    }

}
