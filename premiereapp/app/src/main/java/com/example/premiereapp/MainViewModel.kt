package com.example.premiereapp
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
class MainViewModel : ViewModel(){
    val movies = MutableStateFlow<List<Movie>>(listOf())

    val moviesdetails = MutableStateFlow(MoviesDetail())

    val seriedetail = MutableStateFlow(MoviesDetail())

    // Pour obtenir la valeur actuelle du MutableStateFlow
    val person = MutableStateFlow<List<Person>>(listOf())

    val serie = MutableStateFlow<List<Serie>>(listOf())

    val apikey = "3fa3f7d8ba0a4f2619cd704b65eb8795"

    val language = "fr"

    val service = Retrofit.Builder()
        .baseUrl("https://api.themoviedb.org/3/")
        .addConverterFactory(MoshiConverterFactory.create())
        .build()
        .create(TmdbAPI::class.java)

    fun searchMovies(motcle: String){
        viewModelScope.launch {
            movies.value = service.getFilmsParMotCle(apikey, motcle).results
        }
    }

    fun getWeekMovies(){
        viewModelScope.launch {
            movies.value = service.getFilmsSemaine(apikey,language).results
        }
    }

    fun searchSerie(motcle: String){
        viewModelScope.launch {
            serie.value = service.getSerieParMotCle(apikey, motcle).results
        }
    }

    fun getWeekSerie(){
        viewModelScope.launch {
            serie.value = service.getSerieSemaine(apikey,language).results
        }
    }

    fun getWeekPerson(){
        viewModelScope.launch {
            person.value = service.getActeursSemaine(apikey,language).results
        }
    }

    fun searchPerson(motcle: String){
        viewModelScope.launch {
            person.value = service.getActeursParMotCle(apikey, motcle).results
        }
    }

    fun getFilmsDetails(id: String){
        viewModelScope.launch {
            moviesdetails.value = service.getFilmsDetails(id,apikey,language)
        }
    }

    fun getSerieDetails(id: String){
        viewModelScope.launch {
            seriedetail.value = service.getFilmsDetails(id,apikey,language)
        }
    }

}