package com.example.premiereapp

import android.annotation.SuppressLint
import android.app.Activity
import androidx.compose.animation.core.tween
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Close
import androidx.compose.material.icons.filled.Movie
import androidx.compose.material.icons.filled.Person
import androidx.compose.material.icons.filled.ScreenshotMonitor
import androidx.compose.material.icons.filled.Search
import androidx.compose.material3.BottomAppBar
import androidx.compose.material3.Button
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.FloatingActionButton
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.NavigationRail
import androidx.compose.material3.NavigationRailItem
import androidx.compose.material3.Scaffold
import androidx.compose.material3.SearchBar
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.material3.TextFieldDefaults
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.windowsizeclass.ExperimentalMaterial3WindowSizeClassApi
import androidx.compose.material3.windowsizeclass.WindowSizeClass
import androidx.compose.material3.windowsizeclass.WindowWidthSizeClass
import androidx.compose.material3.windowsizeclass.calculateWindowSizeClass
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.composed
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.exyte.animatednavbar.AnimatedNavigationBar
import com.exyte.animatednavbar.animation.balltrajectory.Parabolic
import com.exyte.animatednavbar.animation.indendshape.Height
import com.exyte.animatednavbar.animation.indendshape.shapeCornerRadius
import com.exyte.animatednavbar.utils.noRippleClickable


@SuppressLint("UnusedMaterial3ScaffoldPaddingParameter")
@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun EcranPrincipal(windowSizeClass: WindowSizeClass){
    Screen(windowClass = windowSizeClass)
}

@Composable
fun Screen(windowClass: WindowSizeClass) {
    when (windowClass.widthSizeClass) {
        WindowWidthSizeClass.Compact -> {
            Column(Modifier.fillMaxSize(),
                verticalArrangement = Arrangement.SpaceEvenly,
                horizontalAlignment = Alignment.CenterHorizontally) {
                TopAndBottomBarr()
            }
        }
        else -> {
            Row(Modifier.fillMaxSize(),
                verticalAlignment = Alignment.CenterVertically) {
                RailBar()
            }
        }
    }
}


@OptIn(ExperimentalMaterial3Api::class)
@SuppressLint("UnusedMaterial3ScaffoldPaddingParameter")
@Composable
fun TopAndBottomBarr(){
    var selectedindex by remember {
        mutableStateOf(0)
    }
    var mytext by remember {
        mutableStateOf("")
    }

    var recherche by remember {
        mutableStateOf(0)
    }

    Scaffold (
        topBar = {SearchBar(activeIndex = selectedindex, {mytext = it}, {recherche = it })},
        bottomBar = {BottomBar( { selectedindex = it})},

        content = {
            // Utilisez un Column pour centrer le contenu au milieu de l'écran
            Column(
                modifier = Modifier
                    .fillMaxSize()
                    .padding(50.dp), // Espacez le contenu du bord de l'écran
                verticalArrangement = Arrangement.spacedBy(8.dp), // Espacez les éléments verticalement
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                Spacer(modifier = Modifier.height(16.dp)) // Ajoute un espace vertical de 16 DP
                // Le composant Film sera centré au milieu
                // FilmSemaine(MainViewModel())
                when (selectedindex) {
                    0 ->if (recherche == 1)
                        RechercheFilms(name = mytext, viewModel = MainViewModel())
                        else
                        FilmSemaine(viewModel = MainViewModel())

                    1 -> if (recherche == 1)
                        RechercheSerie(name = mytext, viewModel = MainViewModel())
                        else
                        SerieSemaine(viewModel = MainViewModel())

                    2 -> if (recherche == 1)
                        RechercheActeurs(name = mytext, viewModel = MainViewModel())
                        else
                        ActeursSemaine(viewModel = MainViewModel())
                    else -> {
                        print("Nothing bla bla !! ")
                    }
                }

                /*
                if (selectedindex == 0 && recherche == 1 ){
                    RechercheFilms(name = mytext, viewModel = MainViewModel())
                }else {
                    FilmSemaine(viewModel = MainViewModel())
                }

                if (selectedindex == 2 && recherche ==1) {
                    RechercheFilms(name = mytext, viewModel = MainViewModel())
                }else {
                    ActeursSemaine(viewModel = MainViewModel())
                }
                if (selectedindex == 2 && recherche == 1) {
                    RechercheActeurs(name = mytext, viewModel = MainViewModel())
                }
                   // RechercheFilms(name = "hobbit", viewModel = MainViewModel())*/


            }
        }

    )
}
@SuppressLint("UnusedMaterial3ScaffoldPaddingParameter")
@Composable
fun ContentWithRail(selectedindex: Int,recherche: Int, mytext: String){
    Scaffold (
        content = {
            // Utilisez un Column pour centrer le contenu au milieu de l'écran
            Row(
                modifier = Modifier
                    .fillMaxSize()
                    .padding(50.dp), // Espacez le contenu du bord de l'écran
            ) {
                Spacer(modifier = Modifier.height(16.dp)) // Ajoute un espace vertical de 16 DP

                when (selectedindex) {
                    0 ->if (recherche == 1)
                        RechercheFilms(name = mytext, viewModel = MainViewModel())
                    else
                        FilmSemaine(viewModel = MainViewModel())

                    1 -> if (recherche == 1)
                        RechercheSerie(name = mytext, viewModel = MainViewModel())
                    else
                        SerieSemaine(viewModel = MainViewModel())

                    2 -> if (recherche == 1)
                        RechercheActeurs(name = mytext, viewModel = MainViewModel())
                    else
                        ActeursSemaine(viewModel = MainViewModel())
                    else -> {
                        print("Nothing bla bla !! ")
                    }
                }            }
        }

    )
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun SearchBar(activeIndex: Int, updateText: (Text: String) -> Unit, updateRecherche: (Recherche: Int) -> Unit){
    var text by remember { mutableStateOf("") }
    var active by remember { mutableStateOf(false) }
    var mytext by remember { mutableStateOf("") }
    var recherche by remember { mutableStateOf(0) }

    // Utilisez l'index actif pour déterminer le texte du placeholder
    val placeholderText = when (activeIndex) {
        0 -> "Recherche de Films"
        1 -> "Recherche de Séries"
        2 -> "Recherche Acteurs "
        else -> "nothing"
    }

    SearchBar(
        modifier = Modifier.fillMaxWidth(),
        query = text,
        onQueryChange = {
            text =it
        },
        onSearch = {
            recherche = 1
            updateRecherche(recherche)
            mytext = text
            updateText(mytext)
            active = false
        },
        active = active,
        onActiveChange = {
            active = it
        },
        placeholder = {
            Text(text = placeholderText)
        },
        leadingIcon = {
            Icon(
                modifier = Modifier.clickable {
                },
                imageVector = Icons.Default.Search,
                contentDescription = "Search Icon"
            )
        },
        trailingIcon = {
            if(active){
                Icon(
                    modifier = Modifier.clickable {
                        if (text.isNotEmpty()){
                            text = ""

                        }else {
                            recherche = 0
                            updateRecherche(recherche)
                            active = false
                        }
                    },
                    imageVector = Icons.Default.Close,
                    contentDescription = "Close Icon"
                )
            }
        }
    ) {

    }

}

@Composable
fun BottomBar( updateIndex: (index: Int) -> Unit){
    val navigationBarItemes = remember{ NavigationBarItems.values() }
    var selectedIndex by remember { mutableStateOf(0) }
    BottomAppBar (modifier = Modifier.fillMaxWidth()){
        AnimatedNavigationBar(
            //modifier = Modifier.height(64.dp),
            selectedIndex = selectedIndex,
            cornerRadius = shapeCornerRadius(cornerRadius = 34.dp),
            ballAnimation = Parabolic(tween(300)),
            indentAnimation = Height(tween(300)),
            ballColor = MaterialTheme.colorScheme.inverseSurface
        ) {
            navigationBarItemes.forEach { item ->
                Box(modifier = Modifier
                    .fillMaxSize()
                    .noRippleClickable {
                        selectedIndex = item.ordinal;
                        updateIndex(selectedIndex)
                    },
                    contentAlignment = Alignment.Center
                ) {
                    Icon(
                        modifier = Modifier.size(60.dp),
                        imageVector  = item.icon,
                        contentDescription = "Bottom Bar Icon",
                        tint = if (selectedIndex == item.ordinal) MaterialTheme.colorScheme.inverseSurface
                        else MaterialTheme.colorScheme.outlineVariant
                    )

                }
            }

        }
    }
}


@Composable
fun RailBar(){
    var selectedItem by remember { mutableIntStateOf(0) }
    val items = listOf("Films", "Séries", "Acteurs")
    val icons = listOf(Icons.Default.Movie, Icons.Default.ScreenshotMonitor, Icons.Default.Person)
    NavigationRail (
    ){
        items.forEachIndexed { index, item ->
            NavigationRailItem(
                icon = {
                    // Personnalisation de l'icône pour centrer et augmenter la taille
                    Box(
                        modifier = Modifier.size(50.dp), // Augmentez la taille de l'icône ici
                        contentAlignment = Alignment.Center
                    ) {
                        Icon(icons[index], contentDescription = item)
                    }
                },
                label = { Text(item) },
                selected = selectedItem == index,
                onClick = { selectedItem = index }
            )
        }
    }
    ContentWithRail(selectedItem,0,"")
}



enum class Page(val title:String, val content: String){
    HOME("home","Show only icon"),
    SEARCH("Search","Show icon with label"),
    SETTINGS("Settings","Show icon /Show the label only when selected")
}
enum class NavigationBarItems(val icon: ImageVector) {
    Person(icon = Icons.Default.Movie),
        Call(icon = Icons.Default.ScreenshotMonitor),
    Setting(icon = Icons.Default.Person),
}

fun Modifier.noRippleClickable(onClick: () -> Unit): Modifier = composed {
    clickable(
        indication =null,
        interactionSource = remember { MutableInteractionSource() }){
        onClick()
    }
}

@OptIn(ExperimentalMaterial3WindowSizeClassApi::class)
@Preview(showBackground = true)
@Composable
fun EcranPrincipalPreview() {
    EcranPrincipal(
        calculateWindowSizeClass(activity = Activity())
    )
}

